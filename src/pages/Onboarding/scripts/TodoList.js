import { ref, watch } from "vue";
import { useQuasar } from "quasar";
import { useRoute, useRouter } from "vue-router";
import VueHighcharts from "vue3-highcharts";
import Filters from "../../../components/Filters.vue";

export default {
  setup() {
    const route = useRoute();
    const router = useRouter();
    let keyResults = ref([{ result_name: null, tasks: [] }]);
    const addKeyResult = () => {
      keyResults.value.push({
        result_name: addKeyResult, // Corrected to include result_name
        tasks: [],
      });
    };
  },
  data() {
    return {
      showTimeInput: false, // Initially hide the time input section
      model: "", // Initialize model for q-time component
    };
  },
  methods: {
    toggleTimeInput() {
      this.showTimeInput = !this.showTimeInput; // Toggle the visibility of time input section
    },
    addKeyResult() {
      // Define your addKeyResult functionality
    },
  },
};
